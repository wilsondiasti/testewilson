# Imagem de exemplo diretamente do Docker https://hub.docker.com/_/gradle
FROM gradle:6.7.1 as builder

# Copiando o script de compilacao e o diretorio da aplicacao
COPY build.gradle .
COPY src ./src

# Construindo o artefato
RUN gradle clean build --no-daemon

# Imagem do JDK diretamente do Docker https://hub.docker.com/_/openjdk
# https://docs.docker.com/develop/develop-images/multistage-build/#use-multi-stage-builds
FROM openjdk:8-jre-alpine

# Copiando o artefato para ser construido no container
COPY --from=builder /home/gradle/build/libs/gradle.jar /helloworld.jar

# Iniciando o container com a aplicacao
CMD [ "java", "-jar", "-Djava.security.egd=file:/dev/./urandom", "/helloworld.jar" ]
